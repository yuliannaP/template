const randomImageWrapper = document.getElementsByClassName("random-image-wrapper")[0];
const randomImageContent = randomImageWrapper.getElementsByClassName("image-content")[0];
const randomImageLoader = randomImageWrapper.getElementsByClassName("loader")[0];
const randomImageErrorMessage = randomImageWrapper.getElementsByClassName("error-message")[0];
const randomImageButton = randomImageWrapper.getElementsByTagName("button")[0];

const byBreedImageWrapper = document.getElementsByClassName("by-breed-image-wrapper")[0];
const byBreedImageContent = byBreedImageWrapper.getElementsByClassName("image-content")[0];
const byBreedImageLoader = byBreedImageWrapper.getElementsByClassName("loader")[0];
const byBreedImageErrorMessage = byBreedImageWrapper.getElementsByClassName("error-message")[0];
const byBreedImageButton = byBreedImageWrapper.getElementsByTagName("button")[0];

const img = document.createElement("img");

const getRandomImageApi = "https://dog.ceo/api/breeds/image/random";
const getBreedsApi = "https://dog.ceo/api/breeds/list/all";

const getRandomImage = () => {
    randomImageContent.innerHTML = "";
    randomImageLoader.innerText = "Loading...";
    return fetch(getRandomImageApi).then((response) => {
        return response.json();
    })
        .catch(err => {
            return err;
        })
        .finally(() => {
            randomImageLoader.innerText = "";
        })
};

const createImageElement = () => {
    getRandomImage().then((response) => {
        img.setAttribute("src", response.message);
        randomImageContent.appendChild(img);
    })
};

randomImageButton.addEventListener("click", createImageElement);


const getBreeds = () => fetch(getBreedsApi).then((response) => response.json());

const getImageByBreeds = (breed) => fetch(`https://dog.ceo/api/breed/${breed}/images/random`).then(response => response.json());

const createImagesList = () => {
    byBreedImageContent.innerHTML = "";
    byBreedImageLoader.innerHTML = "Loading...";
    getBreeds().then(response => {
        const breeds = Object.keys(response.message);
        const breedsList = breeds.map((breed) => {
            return response.message[breed].length ? `${breed}/${response.message[breed][0]}`: breed;
        });
        const promisesList = breedsList.map((breed) => {
            return getImageByBreeds(breed);
        });
        Promise.all(promisesList).then((responses) => {
            byBreedImageLoader.innerHTML = "";
            responses.forEach((response) => {
                const img = document.createElement("img");
                img.setAttribute("src", response.message);
                byBreedImageContent.appendChild(img);
            })
        })

    })
};

byBreedImageButton.addEventListener("click", createImagesList);



